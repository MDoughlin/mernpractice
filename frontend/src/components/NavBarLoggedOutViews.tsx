import { Button } from "react-bootstrap"

interface NavBarLoggedOutViewsProps {
  onSignUpClicked: () => void,
  onLoginClicked: () => void,
}

const NavBarLoggedOutViews = ({ onSignUpClicked, onLoginClicked }: NavBarLoggedOutViewsProps) => {
  return (
    <>
      <Button onClick={onSignUpClicked}>Sign up</Button>
      <Button onClick={onLoginClicked}> Log In</Button>
    </>
  )
}

export default NavBarLoggedOutViews
