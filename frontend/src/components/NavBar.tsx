import { Container, Nav, Navbar } from "react-bootstrap";
import { User } from "../models/user";
import NavBarLoggedInView from "./NavBarLoggedInView";
import NavBarLoggedOutViews from "./NavBarLoggedOutViews";
import { Link } from "react-router-dom";

interface NavBarProps {
  loggedinUser: User | null,
  onSignUpClicked: () => void,
  onLoginClicked: () => void,
  onLogoutSuccessful: () => void,
}

const NavBar = ({ loggedinUser, onSignUpClicked, onLoginClicked, onLogoutSuccessful }: NavBarProps) => {
  return (
    <Navbar bg="primary" variant="dark" expand="lg" sticky="top">
      <Container>
        <Navbar.Brand as={Link} to="/">
          Cool Notes App
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="main-navbar" />
        <Navbar.Collapse id="main-navbar">
          <Nav>
            <Nav.Link as={Link} to="/privacy">
              Privacy
            </Nav.Link>
          </Nav>
          <Nav className="ms-auto">
            {loggedinUser
              ? <NavBarLoggedInView user={loggedinUser} onLogoutSuccessful={onLogoutSuccessful} />
              : <NavBarLoggedOutViews onLoginClicked={onLoginClicked} onSignUpClicked={onSignUpClicked} />
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar
